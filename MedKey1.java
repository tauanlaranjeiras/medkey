package MedKey;

import java.util.Scanner;

public class MedKey1 {

	public static void main(String[] args) {
					
		Scanner sc = new Scanner(System.in);
		System.out.println("Qual seu nome?");
		String nome = sc.next();
		
		System.out.println("Qual sua classe?");
		String classe = sc.next();
		System.out.println("Quais foram suas notas em português?");
		double notaPort1 = sc.nextDouble();
		double notaPort2 = sc.nextDouble();
		double notaPort3 = sc.nextDouble();
		System.out.println("Quais foram suas notas em inglês?");
		double notaIng1 = sc.nextDouble();
		double notaIng2 = sc.nextDouble();
		double notaIng3 = sc.nextDouble();
		System.out.println("Quais foram suas notas em história?");
		double notaHist1 = sc.nextDouble();
		double notaHist2 = sc.nextDouble();
		double notaHist3 = sc.nextDouble();
		System.out.println("Quais foram suas notas em geografia?");
		double notaGeo1 = sc.nextDouble();
		double notaGeo2 = sc.nextDouble();
		double notaGeo3 = sc.nextDouble();
		
		double NP = ((notaPort1+notaPort2+notaPort3)/3);
		double NI = ((notaIng1+notaIng2+notaIng3)/3);
		double NH = ((notaHist1+notaHist2+notaHist3)/3);
		double NG = ((notaGeo1+notaGeo2+notaGeo3)/3);
		
		System.out.println("Nome: "+ nome);
		System.out.println("Classe: " + classe);
		System.out.printf("Média em português: " + "%.2f", NP);
		System.out.println(" ");
		System.out.printf("Média em inglês: " + "%.2f", NI);
		System.out.println(" ");
		System.out.printf("Média em história: " +"%.2f", NH);
		System.out.println(" ");
		System.out.printf("Média em geografia: " +"%.2f", NG);
		System.out.println(" ");
		
		

	}

}
