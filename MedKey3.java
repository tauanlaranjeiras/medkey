package MedKey;

import java.util.Scanner;

public class MedKey3 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Quer calcular a área de qual figura geométrica?" +"\n(1) Quadrado"+"\n(2) Retângulo"+"\n(3) Triângulo"+"\n(4) Círculo");
		int figura = sc.nextInt();
		double PI = 3.14;
		
		if(figura == 1) {
			System.out.println("Informe quanto tem o lado do quadrado: ");
			int LQ = sc.nextInt();
			// LQ = lado do quadrado
			int ADQ = LQ*LQ;
			//ADQ = área do quadrado
			System.out.println("Área do quadrado: " + ADQ + " cm²");
		}
		if(figura == 2) {
			System.out.println("Informe quanto tem a base e a altura do retângulo: ");
			int brt = sc.nextInt(), lrt = sc.nextInt();
			// brt = base do retângulo, lrt = lado do retângulo			
			int ADR = brt*lrt;
			//ADR = área do retângulo
			System.out.println("Área do retângulo: " + ADR + " cm²");
		}
		if(figura == 3) {
			System.out.println("Informe quanto tem a base e a altura do triângulo: ");
			int btr = sc.nextInt(), ltr = sc.nextInt();
			// btr = base do triângulo, ltr = lado do triângulo			
			int ATR = ((btr*ltr)/2);
			//ATR = área do triângulo
			System.out.println("Área do triângulo: " + ATR + " cm²");
		}
		if(figura == 4) {
			System.out.println("Informe quanto tem o raio do círculo: ");
			int r = sc.nextInt();
			// r = raio	
			double ACR = (PI*Math.pow(r, 2));
			//Acr = área do círculo
			System.out.println("Área do círculo: " + ACR + " cm²");
		}
	}

}
